using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using AvaloniaApplication1.Data;
using AvaloniaApplication1.Properties.Langs;
using System.Globalization;

namespace AvaloniaApplication1.Views
{
    public partial class MainWindow : Window
    {
        private Popup PopupConfig => this.FindControl<Popup>("PopupConfig");
        private TextBlock HW => this.FindControl<TextBlock>("HW");

        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void ButtonConfig_OnClick(object sender, RoutedEventArgs e) => PopupConfig.IsOpen = true;

        private void ButtonLangs_OnClick(object sender, RoutedEventArgs e)
        {
            if (e.Source is Button button && button.Tag is string langName)
            {
                PopupConfig.IsOpen = false;
                LocalizedLangExtension.SetLocalLanguage(langName);
            }
        }
    }
}
