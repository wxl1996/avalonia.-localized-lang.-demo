using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AvaloniaApplication1.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Welcome to Avalonia!";
        public Task<string> MyAsyncText => GetTextAsync();

        private async Task<string> GetTextAsync()
        {
            await Task.Delay(1000); // The delay is just for demonstration purpose
            return "Hello from async operation";
        }
    }
}
